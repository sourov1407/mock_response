package com.example.demo;

import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.awt.*;

@SpringBootApplication
public class DemoApplication {
    @Autowired
    private static Environment env;

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);

        int port = Integer.parseInt(ctx.getEnvironment().getProperty("mockserver.port"));
        String host = ctx.getEnvironment().getProperty("mockserver.host");
        ClientAndServer.startClientAndServer(port);
        MockServerClient client = new MockServerClient(host, port);
        ExpectationInitializerExample example = new ExpectationInitializerExample();
        example.initializeExpectations(client);


    }

}
