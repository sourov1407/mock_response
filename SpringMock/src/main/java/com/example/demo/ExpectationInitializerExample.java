package com.example.demo;

import org.mockserver.client.MockServerClient;
import org.mockserver.client.initialize.PluginExpectationInitializer;
import org.mockserver.mock.action.ExpectationResponseCallback;
import org.mockserver.model.Header;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;

import java.util.concurrent.TimeUnit;

import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class ExpectationInitializerExample implements PluginExpectationInitializer {

//    @Override
//    public  Expectation[] initializeExpectations() {
//        return new Expectation[]{
//                new Expectation(
//                        request()
//                                .withPath("/simpleFirst")
//                )
//                        .thenRespond(
//                        response()
//                                .withBody("some first response")
//                ),
//                new Expectation(
//                        request()
//                                .withPath("/simpleSecond")
//                )
//                        .thenRespond(
//                        response()
//                                .withBody("some second response")
//                )
//        };
//    }

    @Override
    public void initializeExpectations(MockServerClient mockServerClient) {
        mockServerClient
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/([a-z/]*)mid/1")
                )
                .respond(
                        response()
                                .withBody("some_response_body")
                                .withDelay(TimeUnit.SECONDS,5)
                );
        mockServerClient
                .when(
                        request()
                        .withPath("/([a-z/]*)mid/2")
                )
                .respond(
                        response()
                        .withBody("{\n" +
                                "\t\"name\" : \"sourov\",\n" +
                                "\t\"Id\" : \"X\"\n" +
                                "}")
                        .withStatusCode(408)
                        .withHeaders(
                                new Header("Content-Type","application/json")
                        )
                );

        //------ Callback

        mockServerClient
                .when(
                        request()
                        .withPath("/([a-z/]*)mid/3")
                )
                .respond(
                        new ExpectationResponseCallback() {
                            @Override
                            public HttpResponse handle(HttpRequest httpRequest) throws Exception {
//                                String path = httpRequest.getPath().getValue();
//                                String id = httpRequest.getFirstQueryStringParameter("name");
                                int num=Integer.parseInt(httpRequest.getFirstHeader("Id"));
                                return response()
//                                        .withBody("Hello 3 "+num);
                                .withBody(
                                        "<licenseCreateRequest>\n" +
                                        "  <licensetemplateId>fc093880-31f6-11ea-9879-b8ac6f151a7d</licensetemplateId>\n" +
                                        "  <externalSystemId>"+num+"</externalSystemId>\n" +
                                        "  <externalProviderId>400</externalProviderId>\n" +
                                        "</licenseCreateRequest>")
                                        .withHeaders(
                                                new Header("Content-Type" , "application/xml; charset=utf-8")
                                        );


                            }
                        }
                );
    }

//    public static void main(String[] args) {
//        System.setProperty("mockserver.initializationClass", ExpectationInitializerExample.class.getName());
//    }
}
